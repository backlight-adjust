.SUFFIXES:
.SUFFIXES: .o .c .l

CFLAGS ?=
LDFLAGS ?=

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(PREFIX)/share/man

CFLAGS += -std=c99 -D_POSIX_C_SOURCE=200809L

# Debug
# CFLAGS += -g -O0 -pedantic -Wall -Wextra -Werror

PROG = bl

BACKLIGHT_PATH=\"/sys/class/backlight/backlight/brightness\"
CFLAGS += -DBACKLIGHT_PATH=$(BACKLIGHT_PATH)

default: all

.PHONY: all
all: $(PROG)

$(PROG): bl.o
	$(CC) -o $@ $^ $(LDFLAGS)

bl.o: bl.c
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	find -name '*.o' -delete
	find -name '*~' -delete
	rm -f $(PROG)

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f $(PROG) $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f bl.1 $(DESTDIR)$(MANDIR)/man1/

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f $(PROG)
	cd $(DESTDIR)$(MANDIR)/man1 && rm -f bl.1
