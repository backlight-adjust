/*
 * Copyright (c) 2017, S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#define STR2(x) #x
#define STR(x) STR2(x)
#define L(x) __FILE__ ":" STR(__LINE__) ": " x

static int16_t brightness;

#define STEP 2

#define BAR_WIDTH 72
#define BAR_PRE " ┝"
#define BAR_POST "┤ "
#define FINAL "\033[G"
#define BRIGHT_CELL "━"
#define DARK_CELL "─"

static void output(void)
{
        printf("%s%s", "\033[G", BAR_PRE);

        for (int j = 0; j < BAR_WIDTH; ++j) {
                if (j * 128 <= brightness * BAR_WIDTH) {
                        printf("%s", BRIGHT_CELL);
                } else {
                        printf("%s", DARK_CELL);
                }
        }

        printf("%s%d/128   %s", BAR_POST, (int) brightness, FINAL);
        fflush(stdout);
}

int main(void)
{
        int ret = 0;
        FILE *sf = 0;
        struct termios in_t = { 0 };
        struct termios stored_in_t = { 0 };
        struct termios stored_out_t = { 0 };
        int in_fd = fileno(stdin);
        int out_fd = fileno(stdout);
        char buf[2];
        int c;

        errno = 0;

        /* Get the /sys entry to manipulate */
        if (!(sf = fopen(BACKLIGHT_PATH, "w+"))) {
                ret = errno;
                perror(L("fopen(\"" BACKLIGHT_PATH "\", \"a+\")"));
                goto err_pre_tty;
        }

        /* Read initial backlight value */
        buf[0] = '\0';
        brightness = 0;

        while (!feof(sf) &&
               !ferror(sf)) {
                if (fread(buf, 1, 1, sf) > 0) {
                        if (buf[0] <= '9' &&
                            buf[0] >= '0') {
                                brightness = brightness * 10 + (buf[0] - '0');
                        }
                }
        }

        if (brightness < 0 ||
            brightness > 128) {
                fprintf(stderr, L("impossible backlight value\n"));
                goto err_pre_tty;
        }

        /* Set up the terminal for pretty */
        if (!isatty(in_fd)) {
                ret = errno;
                fprintf(stderr, L("stdin is not a tty\n"));
                goto err_pre_tty;
        }

        if (tcgetattr(in_fd, &stored_in_t)) {
                ret = errno;
                perror(L("tcgetattr"));
                goto err_pre_tty;
        }

        memcpy(&in_t, &stored_in_t, sizeof in_t);

        if (!isatty(out_fd)) {
                ret = errno;
                fprintf(stderr, L("stdout is not a tty\n"));
                goto err_pre_tty;
        }

        if (tcgetattr(out_fd, &stored_out_t)) {
                ret = errno;
                perror(L("tcgetattr"));
                goto err_pre_tty;
        }

        /* cfmakeraw() is nonstandard, and I want ^C anyway. */
        in_t.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR |
                          ICRNL | IXON);
        in_t.c_oflag &= ~OPOST;
        in_t.c_lflag &= ~(ECHO | ECHONL | ICANON);
        in_t.c_cflag &= ~(CSIZE | PARENB);
        in_t.c_cflag |= CS8;

        /* Technically, it is necessary to check these one by one. That's a pain */
        if (tcsetattr(in_fd, TCSANOW, &in_t)) {
                ret = errno;
                perror(L("tcsetattr"));
                goto err_pre_tty;
        }

        printf("\033[?25l");
        output();

        while ((c = getchar()) != EOF) {
                switch (c) {
                case 'q':
                case 'Q':
                        goto done;
                        break;
                case 005:
                case 'j':
                case 'J':
                        brightness -= (2 * STEP);
                case 031:
                case 'k':
                case 'K':
                        brightness += STEP;

                        if (brightness < 0) {
                                brightness = 0;
                        } else if (brightness > 128) {
                                brightness = 128;
                        }

                        fprintf(sf, "%d\n", (int) brightness);
                        fflush(sf);
                        break;
                default:
                        break;
                }

                output();
        }

done:
        tcsetattr(in_fd, TCSANOW, &stored_in_t);
        printf("\033[?25h\n");
err_pre_tty:

        if (sf) {
                fclose(sf);
        }

        return ret;
}
